$(function() {
    $(window).scroll(function () {
        var t = $(this).scrollTop();
        var t2 = $("#navi").offset().top;
        $('.pg').text(t + "/" + t2);
        $("#navi").css('top', t);
    });
    
    $(":button[name='checkout']").click(function() {
        var item = $("#navi div.selected_item div.hoge div.title span.name");
        var size = $("#navi div.selected_size div");
        
        var txt = "";
        if (item != null) txt = "種類:" + item.text();
        if (size != null) txt = txt + " サイズ:" + size.text();
        
        alert(txt);
    });
    
    var org = $(".hoge").clone().show();
    $.each(STATIC_DATA.items, function(i, val){
        var hoge = org.clone();
        
        hoge.click(function(event) {
            $("#navi div.selected_item *").remove();
            $("#navi div.selected_item").append( hoge.clone() );
        });
        
        $("div.title span.name", hoge).text(val.name);
        $("div.title span.code", hoge).text(val.code);
        $("div.image img.preview", hoge).attr('src', val.url);
        $("#dest").append(hoge);
    });
    
    org = $(".piyo").clone().show();
    $.each(STATIC_DATA.sizes, function(i, val) {
        var piyo = org.clone();
        
        piyo.click(function(event) {
            $("#navi div.selected_size *").remove();
            $("#navi div.selected_size").append( $("<div>").text($(":radio[name='select-size']:checked").val()) );
        });
        
        $(":radio[name='select-size']", piyo).val(val.name);
        $("span.select-caption", piyo).text(val.name);
        $("#dest-select").append(piyo);
    });
});