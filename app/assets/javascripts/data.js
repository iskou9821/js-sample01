window.STATIC_DATA = {};
STATIC_DATA.items = [
    {code: 'test1', name : 'テスト1', url: 'http://www.nitori-net.jp/img/goods/92/7327838.jpg'},
    {code: 'test2', name : 'テスト2', url: 'http://www.nitori-net.jp/img/goods/92/7327552.jpg'},
    {code: 'test3', name : 'テスト3', url: 'http://www.nitori-net.jp/img/goods/92/7327883.jpg'},
    {code: 'test4', name : 'テスト4', url: 'http://www.nitori-net.jp/img/goods/92/7327527.jpg'},
    {code: 'test5', name : 'テスト5', url: 'http://www.nitori-net.jp/img/goods/92/7327527.jpg'},
    {code: 'test6', name : 'テスト6', url: 'http://www.nitori-net.jp/img/goods/92/7327485.jpg'}
];

STATIC_DATA.sizes = [
    { name : '100cm x 110cm' },
    { name : '100cm x 140cm' },
    { name : '100cm x 178cm' },
    { name : '100cm x 190cm' },
    { name : '100cm x 200cm' },
    { name : '100cm x 210cm' },
    { name : '100cm x 220cm' }    
];